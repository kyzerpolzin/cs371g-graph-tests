// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph>;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> edAB = add_edge(vdA, vdB, g);

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);

    ASSERT_NE(edAB, p1);
    ASSERT_EQ(p1.first,  edAB.first);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB.first);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB.first, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB.first, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;

    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

/**tests whether the graph is empty if adding no vertices/edges
 * Checks num_edges and num_vertices Functions
 */
TYPED_TEST(GraphFixture, test5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edges_size_type = typename TestFixture::edges_size_type;

    graph_type g;

    vertices_size_type numV = num_vertices(g);
    edges_size_type numE = num_edges(g);

    ASSERT_EQ(numV, 0);
    ASSERT_EQ(numE, 0);

}

/**
 * Tests adding vertices and edges in a graph
 * Tests add_vertex/edge and num_vertices/Edges
 *
 */
TYPED_TEST(GraphFixture, test6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edges_size_type = typename TestFixture::edges_size_type;

    graph_type g;


    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    vertices_size_type numV = num_vertices(g);
    edges_size_type numE = num_edges(g);

    ASSERT_EQ(numV, 4);
    ASSERT_EQ(numE, 0);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdD, g);
    add_edge(vdD, vdA, g);

    numE = num_edges(g);

    ASSERT_EQ(numE, 4);
}

/**
 * Tests adding vertices and then checking they are in the correct vertex position
 * Tests add_vertex and vertex function
 *
 */
TYPED_TEST(GraphFixture, test7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;


    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    vertex_descriptor vdA1 = vertex(0, g);
    vertex_descriptor vdB1 = vertex(1, g);
    vertex_descriptor vdC1 = vertex(2, g);
    vertex_descriptor vdD1 = vertex(3, g);

    ASSERT_EQ(vdA1, vdA);
    ASSERT_EQ(vdB1, vdB);
    ASSERT_EQ(vdC1, vdC);
    ASSERT_EQ(vdD1, vdD);
}

/**
 * Tests adding vertices and edges and then checking that edges are valid
 * Tests add_vertex/edge and edge function
 *
 */
TYPED_TEST(GraphFixture, test8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;


    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);


    add_edge(vdA, vdB, g).first;
    add_edge(vdB, vdC, g).first;
    add_edge(vdC, vdD, g).first;
    add_edge(vdD, vdA, g).first;


    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = edge(vdB, vdC, g);
    pair<edge_descriptor, bool> e3 = edge(vdC, vdD, g);
    pair<edge_descriptor, bool> e4 = edge(vdD, vdA, g);

    ASSERT_EQ(e1.second, true);
    ASSERT_EQ(e2.second, true);
    ASSERT_EQ(e3.second, true);
    ASSERT_EQ(e4.second, true);
}


/**
 * Tests the that the graph is directed
 * Tests add_vertex/edge and edge functions
 *
 */
TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;


    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    add_edge(vdA, vdB, g).first;
    add_edge(vdB, vdC, g).first;
    add_edge(vdC, vdD, g).first;
    add_edge(vdD, vdA, g).first;

    pair<edge_descriptor, bool> e1 = edge(vdB, vdA, g);
    pair<edge_descriptor, bool> e2 = edge(vdC, vdB, g);
    pair<edge_descriptor, bool> e3 = edge(vdD, vdC, g);
    pair<edge_descriptor, bool> e4 = edge(vdA, vdD, g);


    ASSERT_EQ(e1.second, false);
    ASSERT_EQ(e2.second, false);
    ASSERT_EQ(e3.second, false);
    ASSERT_EQ(e4.second, false);
}

/**
 * Tests the sources and targets of edges since graph is directed
 * Tests add_vertex/edge and source/target functions
 *
 */
TYPED_TEST(GraphFixture, test10) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;


    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);


    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCD = add_edge(vdC, vdD, g).first;
    edge_descriptor edDA = add_edge(vdD, vdA, g).first;

    vertex_descriptor vdA1 = source(edAB, g);
    vertex_descriptor vdB1 = source(edBC, g);
    vertex_descriptor vdC1 = source(edCD, g);
    vertex_descriptor vdD1 = source(edDA, g);

    ASSERT_EQ(vdA1, vdA);
    ASSERT_EQ(vdB1, vdB);
    ASSERT_EQ(vdC1, vdC);
    ASSERT_EQ(vdD1, vdD);
    ASSERT_NE(vdA1, vdB);

    vertex_descriptor vA1 = target(edAB, g);
    vertex_descriptor vB1 = target(edBC, g);
    vertex_descriptor vC1 = target(edCD, g);
    vertex_descriptor vD1 = target(edDA, g);

    ASSERT_EQ(vA1, vdB);
    ASSERT_EQ(vB1, vdC);
    ASSERT_EQ(vC1, vdD);
    ASSERT_EQ(vD1, vdA);
    ASSERT_NE(vA1, vdA);
}

/**
 * Tests the sources and targets of edges since graph is directed
 * Tests add_vertex/edge and source/target functions
 *
 */
TYPED_TEST(GraphFixture, test11) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;


    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);


    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCD = add_edge(vdC, vdD, g).first;
    edge_descriptor edDA = add_edge(vdD, vdA, g).first;

    vertex_descriptor vdA1 = source(edAB, g);
    vertex_descriptor vdB1 = source(edBC, g);
    vertex_descriptor vdC1 = source(edCD, g);
    vertex_descriptor vdD1 = source(edDA, g);

    ASSERT_EQ(vdA1, vdA);
    ASSERT_EQ(vdB1, vdB);
    ASSERT_EQ(vdC1, vdC);
    ASSERT_EQ(vdD1, vdD);
    ASSERT_NE(vdA1, vdB);

    vertex_descriptor vA1 = target(edAB, g);
    vertex_descriptor vB1 = target(edBC, g);
    vertex_descriptor vC1 = target(edCD, g);
    vertex_descriptor vD1 = target(edDA, g);

    ASSERT_EQ(vA1, vdB);
    ASSERT_EQ(vB1, vdC);
    ASSERT_EQ(vC1, vdD);
    ASSERT_EQ(vD1, vdA);
    ASSERT_NE(vA1, vdA);
}

/**
 * Tests that trying to add a duplicate edge fails (pair has false as second element)
 * Tests add_vertex/edge functions
 *
 */
TYPED_TEST(GraphFixture, test12) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> result = add_edge(vdA, vdB, g);

    ASSERT_TRUE(result.second);

    pair<edge_descriptor, bool> result2 = add_edge(vdA, vdB, g);

    ASSERT_FALSE(result2.second);

    ASSERT_EQ(result.first, result2.first);
}

/**
 * Tests that the vertex itself isn't in its adjacency list (if no edge)
 * Tests add_vertex and adjacent_vertices functions
 */
TYPED_TEST(GraphFixture, test13) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<adjacency_iterator, adjacency_iterator> result = adjacent_vertices(vdA, g);

    //implies list is empty
    ASSERT_EQ(result.first, result.second);
}

/**
 * Tests that the vertex can have itself in its adjacency list
 * Tests add_vertex/edge and adjacent_vertices functions
 */
TYPED_TEST(GraphFixture, test14) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor    = typename TestFixture::edge_descriptor;
    using adjacency_iterator   = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<edge_descriptor, bool> ed1 = add_edge(vdA, vdA, g);

    ASSERT_TRUE(ed1.second);

    pair<adjacency_iterator, adjacency_iterator> res = adjacent_vertices(vdA, g);

    ASSERT_NE(res.first, res.second);

}

/**
 * Tests that add_edge will create vertices if not in graph already
 * Tests num_vertices/edges and add_edge functions
 */
TYPED_TEST(GraphFixture, test15) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_EQ(num_vertices(g), 0);

    add_edge(0, 1, g);

    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_EQ(num_edges(g), 1);
}

/**
 * Another test to ensure directionality
 * Tests add_edge/vertex functions
 */
TYPED_TEST(GraphFixture, test16) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor    = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> ed1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> ed2 = add_edge(vdB, vdA, g);

    ASSERT_EQ(num_edges(g), 2);
    ASSERT_NE(ed1.first, ed2.first);
}

/**
 * Tests the vertex iterator. Ensures it loops through every vertex
 * Tests add_vertex and vertices functions
 */
TYPED_TEST(GraphFixture, test17) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    for (int i = 0; i < 1000; i++)
    {
        add_vertex(g);
    }

    pair<vertex_iterator, vertex_iterator> result = vertices(g);

    vertex_iterator b = result.first;
    vertex_iterator e = result.second;

    for(int i = 0; i < 1000; ++i) {
        ++b;
    }
    ASSERT_EQ(b, e);
}

/**
 * Tests that adjacency list contains no duplicates
 */
TYPED_TEST(GraphFixture, test18) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator   = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    for(int i = 0; i < 10; ++i) {
        add_edge(vdA, vdB, g);
    }

    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_EQ(num_edges(g), 1);

    pair<adjacency_iterator, adjacency_iterator> result = adjacent_vertices(vdA, g);

    adjacency_iterator b = result.first;
    adjacency_iterator e = result.second;

    ++b;

    ASSERT_EQ(b, e);
}

/**
 * Tests that graphs are independent when not connected
 */
TYPED_TEST(GraphFixture, test19) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;
    graph_type g2;

    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 1);
    ASSERT_EQ(num_vertices(g2), 0);
}

/**
 * Similar test as above but emphasizes both are able to contain vertices at same time (aka independent objects)
 */
TYPED_TEST(GraphFixture, test20) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;
    graph_type g2;

    add_vertex(g);
    add_vertex(g2);

    ASSERT_EQ(num_vertices(g), 1);
    ASSERT_EQ(num_vertices(g2), 1);
}

/**
 * Tests trying to add vertex from other graph (should simply create new vertex in new graph that represents other vertex)
 */
TYPED_TEST(GraphFixture, test21) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g2);

    pair<edge_descriptor, bool> result = add_edge(vdA, vdB, g1);
    pair<edge_descriptor, bool> result2 = add_edge(vdB, vdA, g2);

    ASSERT_EQ(result.second, true);
    ASSERT_EQ(result2.second, true);
}

/**
 * Tests edge iterator when there is no edges
 */
TYPED_TEST(GraphFixture, test22) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_iterator    = typename TestFixture::edge_iterator;

    graph_type g;

    add_vertex(g);
    add_vertex(g);

    pair<edge_iterator, edge_iterator> result = edges(g);

    ASSERT_EQ(result.first, result.second);
}

/**
 * Tests edge iterator when there is many edges
 */
TYPED_TEST(GraphFixture, test23) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_iterator     = typename TestFixture::edge_iterator;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    for(int i = 0; i < 1000; ++i) {
        vertex_descriptor vdB = add_vertex(g);
        add_edge(vdA, vdB, g);
        vdA = vdB;
    }

    ASSERT_EQ(num_edges(g), 1000);
    ASSERT_EQ(num_vertices(g), 1001);

    pair<edge_iterator, edge_iterator> result = edges(g);
    edge_iterator b = result.first;
    edge_iterator e = result.second;

    for(int i = 0; i < 1000; ++i) {
        ++b;
    }

    ASSERT_EQ(b, e);
}

/**
 * Tests vertex iterator when there is no vertices
 */
TYPED_TEST(GraphFixture, test24) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;

    pair<vertex_iterator, vertex_iterator> result = vertices(g);

    ASSERT_EQ(result.first, result.second);
}

/**
 * Tests vertex iterator when there is many vertices
 * Should be independent of the edges
 */
TYPED_TEST(GraphFixture, test25) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator     = typename TestFixture::vertex_iterator;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    for(int i = 0; i < 1000; ++i) {
        vertex_descriptor vdB = add_vertex(g);
        add_edge(vdA, vdB, g);
        vdA = vdB;
    }

    ASSERT_EQ(num_edges(g), 1000);
    ASSERT_EQ(num_vertices(g), 1001);

    pair<vertex_iterator, vertex_iterator> result = vertices(g);
    vertex_iterator b = result.first;
    vertex_iterator e = result.second;

    for(int i = 0; i < 1001; ++i) {
        ++b;
    }

    ASSERT_EQ(b, e);
}

/**
 * Tests that adjacency list works on complete graph
 */
TYPED_TEST(GraphFixture, test26) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    using adjacency_iterator   = typename TestFixture::adjacency_iterator;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdA, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdC, vdB, g);

    int cA = 0;
    int cB = 0;
    int cC = 0;
    vertex_iterator b = vertices(g).first;
    vertex_iterator e = vertices(g).second;
    while (b != e) {
        vertex_descriptor i = *b;
        adjacency_iterator ab = adjacent_vertices(i, g).first;
        adjacency_iterator ae = adjacent_vertices(i, g).second;
        while (ab != ae) {
            if (i == vdA) {
                ++cA;
            }
            if (i == vdB) {
                ++cB;
            }
            if (i == vdC) {
                ++cC;
            }
            ++ab;
        }
        ++b;
    }
    ASSERT_TRUE(cA == cB && cB == cC);
}

/**
 * Simple test for source on one edge
 */
TYPED_TEST(GraphFixture, test27) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor result = add_edge(vdA, vdB, g).first;
    ASSERT_EQ(source(result, g), vdA);
}

/**
 * Test for source on many edges
 */
TYPED_TEST(GraphFixture, test28) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    for(int i = 0; i < 1000; ++i) {
        vertex_descriptor vdB = add_vertex(g);
        edge_descriptor result = add_edge(vdA, vdB, g).first;
        ASSERT_EQ(source(result, g), vdA);
        vdA = vdB;
    }
}

/**
 * Simple test for target on one edge
 */
TYPED_TEST(GraphFixture, test29) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor result = add_edge(vdA, vdB, g).first;
    ASSERT_EQ(target(result, g), vdB);
}

/**
 * Test for source on many edges
 */
TYPED_TEST(GraphFixture, test30) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    for(int i = 0; i < 1000; ++i) {
        vertex_descriptor vdB = add_vertex(g);
        edge_descriptor result = add_edge(vdA, vdB, g).first;
        ASSERT_EQ(target(result, g), vdB);
        vdA = vdB;
    }
}

/**
 * Test vertex function on many vertices
 */
TYPED_TEST(GraphFixture, test31) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    for(int i = 0; i < 1000; ++i) {
        ASSERT_EQ(vertex(i, g), vdA);
        vertex_descriptor vdB = add_vertex(g);
        vdA = vdB;
    }
}

/**
 * Test for edge on many edges
 */
TYPED_TEST(GraphFixture, test32) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    for(int i = 0; i < 1000; ++i) {

        vertex_descriptor vdB = add_vertex(g);

        edge_descriptor result = add_edge(vdA, vdB, g).first;

        ASSERT_EQ(edge(vdA, vdB, g).first, result);

        vdA = vdB;
    }
}

/**
 * Test for num_edges/vertices on many edges
 */
TYPED_TEST(GraphFixture, test33) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    for(int i = 0; i < 1000000; ++i) {
        vertex_descriptor vdB = add_vertex(g);
        add_edge(vdA, vdB, g);
        vdA = vdB;
    }
    ASSERT_EQ(num_edges(g), 1000000);
    ASSERT_EQ(num_vertices(g), 1000001);
}

/**
 * Test that dereferencing vertex iterator gives proper vertex
 */
TYPED_TEST(GraphFixture, test34) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> result = vertices(g);

    vertex_iterator b = result.first;
    vertex_iterator e = result.second;

    vertex_descriptor curr = *b;

    ASSERT_EQ(curr, vdA);
    ++b;
    curr = *b;

    ASSERT_EQ(curr, vdB);
    ++b;
    curr = *b;

    ASSERT_EQ(curr, vdC);
    ++b;

    ASSERT_EQ(b, e);
}

/**
 * Testing source when edge is to itself
 */
TYPED_TEST(GraphFixture, test35) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor      = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    edge_descriptor ed1 = add_edge(vdA, vdA, g).first;

    ASSERT_EQ(source(ed1, g), vdA);
}

/**
 * Testing target when edge is to itself
 */
TYPED_TEST(GraphFixture, test36) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor      = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    edge_descriptor ed1 = add_edge(vdA, vdA, g).first;

    ASSERT_EQ(target(ed1, g), vdA);
}

/**
 * Testing source when add_edge creates the vertices
 */
TYPED_TEST(GraphFixture, test37) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor      = typename TestFixture::edge_descriptor;

    graph_type g;

    edge_descriptor ed1 = add_edge(0, 1, g).first;

    ASSERT_EQ(source(ed1, g), vertex(0, g));
}

/**
 * Testing target when add_edge creates the vertices
 */
TYPED_TEST(GraphFixture, test38) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor      = typename TestFixture::edge_descriptor;

    graph_type g;

    edge_descriptor ed1 = add_edge(0, 1, g).first;

    ASSERT_EQ(target(ed1, g), vertex(1, g));
}

/**
 * Testing target and source are equal when an edge is pointing to same vertex
 */
TYPED_TEST(GraphFixture, test39) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    edge_descriptor ed1 = add_edge(vdA, vdA, g).first;

    ASSERT_EQ(target(ed1, g), source(ed1, g));
}

/**
 * Test that dereferencing edge iterator gives proper edge
 *
 * See comment at line 158 in Graph.hpp (this test works if you delete keywork static on line 172)
 */
// TYPED_TEST(GraphFixture, test40) {
//     using graph_type          = typename TestFixture::graph_type;
//     using vertex_descriptor   = typename TestFixture::vertex_descriptor;
//     using edge_descriptor   = typename TestFixture::edge_descriptor;
//     using edge_iterator   = typename TestFixture::edge_iterator;

//     graph_type g;

//     vertex_descriptor vdA = add_vertex(g);
//     vertex_descriptor vdB = add_vertex(g);
//     vertex_descriptor vdC = add_vertex(g);
//     vertex_descriptor vdD = add_vertex(g);

//     //edge_descriptor ab = add_edge(vdA, vdB, g).first;
//     // edge_descriptor bc = add_edge(vdB, vdC, g).first;
//     // edge_descriptor cd = add_edge(vdC, vdD, g).first;


//     pair<edge_iterator, edge_iterator> result = edges(g);

//     edge_iterator b = result.first;
//     edge_iterator e = result.second;

//     // ASSERT_EQ(*b, ab);
//     // ++b;

//     // ASSERT_EQ(*b, bc);
//     // ++b;

//     // ASSERT_EQ(*b, cd);
//     // ++b;

//     ASSERT_EQ(b, e);
// }
